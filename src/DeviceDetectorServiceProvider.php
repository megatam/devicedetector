<?php

namespace Megatam\DeviceDetector;

use Illuminate\Support\ServiceProvider;

class DeviceDetectorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->singleton('DeviceDetector', function ($app) {

            return new DeviceDetector(null,['autoInitOfHttpHeaders'=>true]);
        });

        $this->app->alias('DeviceDetector', DeviceDetector::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['DeviceDetector', DeviceDetector::class];
    }
}
